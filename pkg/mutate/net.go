package mutate

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/okd-webhook/pkg/webhook"
	corev1 "k8s.io/api/core/v1"
)

type NetworkMutator struct{}

// Patch configures pod networking according to
// the OpenShift Restricted SCC.
// (https://docs.openshift.com/container-platform/4.6/authentication/managing-security-context-constraints.html)
func (nm *NetworkMutator) Patch(target *corev1.Pod) (patch []webhook.PatchOperation) {
	log.Infof("networkMutator is patching pod %s/%s", target.Namespace, target.Name)
	patch = append(patch, webhook.PatchOperation{
		Op:    JSONOpReplace,
		Path:  "/spec/hostIPC",
		Value: false,
	})
	patch = append(patch, webhook.PatchOperation{
		Op:    JSONOpReplace,
		Path:  "/spec/hostPID",
		Value: false,
	})
	patch = append(patch, webhook.PatchOperation{
		Op:    JSONOpReplace,
		Path:  "/spec/hostNetwork",
		Value: false,
	})

	return patch
}
