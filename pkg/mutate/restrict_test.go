package mutate

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/okd-webhook/pkg/webhook"
	corev1 "k8s.io/api/core/v1"
	"testing"
)

// interface guard
var rm webhook.Mutation = &RestrictedMutator{}

func TestRestrictedMutator_Patch(t *testing.T) {
	pod := &corev1.Pod{
		Spec: corev1.PodSpec{
			SecurityContext: &corev1.PodSecurityContext{},
			Containers: []corev1.Container{
				{
					SecurityContext: &corev1.SecurityContext{},
				},
			},
		},
	}

	patches := rm.Patch(pod)
	assert.EqualValues(t, 7, len(patches))
}

func TestRestrictedIgnoresCorrectValues(t *testing.T) {
	f := false
	pod := &corev1.Pod{
		Spec: corev1.PodSpec{
			SecurityContext: &corev1.PodSecurityContext{},
			Containers: []corev1.Container{
				{
					SecurityContext: &corev1.SecurityContext{
						AllowPrivilegeEscalation: &f,
					},
				},
			},
		},
	}

	patches := rm.Patch(pod)
	assert.EqualValues(t, 6, len(patches))
}

func TestRestrictedMutator_addOrReplace(t *testing.T) {
	rm := &RestrictedMutator{}

	assert.EqualValues(t, JSONOpAdd, rm.addOrReplace(true, "", nil).Op)
	assert.EqualValues(t, JSONOpReplace, rm.addOrReplace(false, "", nil).Op)
}
