package mutate

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/okd-webhook/pkg/webhook"
	corev1 "k8s.io/api/core/v1"
	"testing"
)

// interface guard
var nm webhook.Mutation = &NetworkMutator{}

func TestNetworkMutator_Patch(t *testing.T) {
	pod := &corev1.Pod{}

	patches := nm.Patch(pod)
	assert.EqualValues(t, 3, len(patches))
}
