package mutate

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/okd-webhook/pkg/webhook"
	corev1 "k8s.io/api/core/v1"
)

var (
	MetricsRestrictPrivileged = promauto.NewCounter(prometheus.CounterOpts{
		Name: "scc_privileged_blocked_total",
		Help: "Total number of times the 'privileged' flag has been blocked",
	})
	MetricsRestrictPrivEsc = promauto.NewCounter(prometheus.CounterOpts{
		Name: "scc_privesc_blocked_total",
		Help: "Total number of times the 'allowPrivilegeEscalation' flag has been blocked",
	})
)

type RestrictedMutator struct {
	RunAsUserMin int64
	RunAsUserMax int64
	RunAsGroup   int64
	FSGroup      int64
}

// Patch configures the Pod and Container SecurityContext
// according to the OpenShift Restricted SCC.
// (https://docs.openshift.com/container-platform/4.6/authentication/managing-security-context-constraints.html)
func (rm *RestrictedMutator) Patch(target *corev1.Pod) (patch []webhook.PatchOperation) {
	log.Infof("restrictedMutator is patching pod %s/%s", target.Namespace, target.Name)
	sc := target.Spec.SecurityContext
	if sc == nil {
		patch = append(patch, webhook.PatchOperation{
			Op:    JSONOpAdd,
			Path:  "/spec/securityContext",
			Value: corev1.PodSecurityContext{},
		})
	}
	// enforce the custom UID
	var uid *int64
	if sc != nil && sc.RunAsUser != nil {
		log.Debugf("detected existing /spec/securityContext/runAsUser (%d)", *sc.RunAsUser)
		uid = sc.RunAsUser
	}
	if uid == nil || *uid > rm.RunAsUserMax || *uid < rm.RunAsUserMin {
		log.WithFields(log.Fields{
			"expected": fmt.Sprintf("%d > x > %d", rm.RunAsUserMax, rm.RunAsUserMin),
			"actual":   uid,
		}).Debug("patching /spec/securityContext/runAsUser")
		patch = append(patch, rm.addOrReplace(sc == nil || sc.RunAsUser == nil, "/spec/securityContext/runAsUser", &rm.RunAsUserMin))
	}
	// enforce the custom GID
	patch = append(patch, rm.addOrReplace(sc == nil || sc.RunAsGroup == nil, "/spec/securityContext/runAsGroup", &rm.RunAsGroup))
	// enforce the custom fsGroup
	patch = append(patch, rm.addOrReplace(sc == nil || sc.FSGroup == nil, "/spec/securityContext/fsGroup", &rm.FSGroup))
	// block running as root
	patch = append(patch, rm.addOrReplace(sc == nil || sc.RunAsNonRoot == nil, "/spec/securityContext/runAsNonRoot", true))
	// configure container-level security context
	for i := range target.Spec.Containers {
		patch = append(patch, rm.patchContainer(i, &target.Spec.Containers[i])...)
	}
	return patch
}

// patchContainer returns a list of patches for a given Container
func (rm *RestrictedMutator) patchContainer(idx int, target *corev1.Container) (patch []webhook.PatchOperation) {
	log.Infof("restrictedMutator is patching container '%s' (%d)", target.Name, idx)
	sc := target.SecurityContext
	if sc == nil {
		patch = append(patch, webhook.PatchOperation{
			Op:    JSONOpAdd,
			Path:  fmt.Sprintf("/spec/containers/%d/securityContext", idx),
			Value: corev1.SecurityContext{},
		})
	}
	// disable privilege escalation
	if sc == nil || sc.AllowPrivilegeEscalation == nil || *sc.AllowPrivilegeEscalation {
		MetricsRestrictPrivEsc.Inc()
		log.WithFields(log.Fields{
			"expected": false,
		}).Debug("patching /spec/containers/securityContext/allowPrivilegeEscalation")
		patch = append(patch, rm.addOrReplace(sc == nil || sc.AllowPrivilegeEscalation == nil, fmt.Sprintf("/spec/containers/%d/securityContext/allowPrivilegeEscalation", idx), false))
	}
	// disable privileged containers
	if sc == nil || sc.Privileged == nil || *sc.Privileged {
		MetricsRestrictPrivileged.Inc()
		log.WithFields(log.Fields{
			"expected": false,
		}).Debug("patching /spec/containers/securityContext/privileged")
		patch = append(patch, rm.addOrReplace(sc == nil || sc.Privileged == nil, fmt.Sprintf("/spec/containers/%d/securityContext/privileged", idx), false))
	}
	// block modification of capabilities
	patch = append(patch, rm.addOrReplace(target.SecurityContext == nil || target.SecurityContext.Capabilities == nil, fmt.Sprintf("/spec/containers/%d/securityContext/capabilities", idx), &corev1.Capabilities{
		Add: []corev1.Capability{},
		Drop: []corev1.Capability{
			"KILL",
			"MKNOD",
			"SYS_CHROOT",
			"SETUID",
			"SETGID",
		},
	}))
	return patch
}

func (*RestrictedMutator) addOrReplace(add bool, path string, value interface{}) webhook.PatchOperation {
	var op = JSONOpReplace
	if add {
		op = JSONOpAdd
	}
	return webhook.PatchOperation{
		Op:    op,
		Path:  path,
		Value: value,
	}
}
