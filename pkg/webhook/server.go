package webhook

import (
	"context"
	"encoding/json"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	v1 "k8s.io/api/admission/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"net/http"
)

var ignoredNamespaces = []string{
	metav1.NamespaceSystem,
	metav1.NamespacePublic,
}

var (
	runtimeScheme = runtime.NewScheme()
	codecs        = serializer.NewCodecFactory(runtimeScheme)
	deserializer  = codecs.UniversalDeserializer()
)

var (
	MetricMutationSkipped = promauto.NewCounter(prometheus.CounterOpts{
		Name: "scc_webhook_mutation_skipped_total",
		Help: "Total number of times a mutation has been skipped",
	})
	MetricMutationDryRun = promauto.NewCounter(prometheus.CounterOpts{
		Name: "scc_webhook_mutation_dry_run_total",
		Help: "Total number of times a mutation has been skipped due to DryRun",
	})
	MetricMutationBadRequest = promauto.NewCounter(prometheus.CounterOpts{
		Name: "scc_webhook_mutation_bad_request_total",
		Help: "Total number of bad requests received",
	})
)

type Server struct {
	mutations []Mutation
}

func NewServer(mutations ...Mutation) *Server {
	wh := new(Server)
	wh.mutations = mutations

	return wh
}

// isMutationAllowed checks whether we should even bother modifying the object
func (wh *Server) isMutationAllowed(ctx context.Context, metadata *metav1.ObjectMeta) bool {
	id := tracer.GetContextId(ctx)
	// skip special kubernetes system namespaces
	for _, namespace := range ignoredNamespaces {
		if metadata.Namespace == namespace {
			log.WithFields(log.Fields{
				"name":      metadata.Name,
				"namespace": metadata.Namespace,
				"id":        id,
			}).Info("skipping mutation as this is a special namespace")
			return false
		}
	}
	return true
}

// createPatch generates the JSON list of patches to apply to the pod
func (wh *Server) createPatch(ctx context.Context, pod *corev1.Pod) ([]byte, error) {
	id := tracer.GetContextId(ctx)
	var patch []PatchOperation

	// iterate our mutators and get their patches
	var ops []PatchOperation
	for i, m := range wh.mutations {
		ops = m.Patch(pod)
		log.WithField("id", id).Debugf("generated %d patches from mutator %d", len(ops), i)
		patch = append(patch, ops...)
	}

	return json.Marshal(patch)
}

func (wh *Server) mutate(ctx context.Context, ar *v1.AdmissionReview) *v1.AdmissionResponse {
	id := tracer.GetContextId(ctx)
	req := ar.Request
	var pod corev1.Pod
	if err := json.Unmarshal(req.Object.Raw, &pod); err != nil {
		MetricMutationBadRequest.Inc()
		log.WithError(err).WithField("id", id).Error("failed to unmarshal raw object")
		return &v1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	}
	log.WithField("id", id).Infof("AdmissionReview for Kind=%v, Namespace=%v Name=%v (%v) UID=%v patchOperation=%v UserInfo=%v",
		req.Kind, req.Namespace, req.Name, pod.Name, req.UID, req.Operation, req.UserInfo)

	if *req.DryRun {
		MetricMutationSkipped.Inc()
		MetricMutationDryRun.Inc()
		log.WithField("id", id).Infof("skipping mutation for %s/%s due to dryRun being true", pod.Namespace, pod.Name)
		return &v1.AdmissionResponse{
			Allowed: true,
		}
	}

	// determine whether to perform mutation
	if !wh.isMutationAllowed(ctx, &pod.ObjectMeta) {
		log.WithField("id", id).Infof("skipping mutation for %s/%s due to policy check", pod.Namespace, pod.Name)
		MetricMutationSkipped.Inc()
		return &v1.AdmissionResponse{
			Allowed: true,
		}
	}

	patchBytes, err := wh.createPatch(ctx, &pod)
	if err != nil {
		return &v1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	}

	log.WithField("id", id).Infof("AdmissionResponse: patch=%v\n", string(patchBytes))
	return &v1.AdmissionResponse{
		Allowed: true,
		Patch:   patchBytes,
		PatchType: func() *v1.PatchType {
			pt := v1.PatchTypeJSONPatch
			return &pt
		}(),
	}
}

// ServeHTTP method for webhook webhook
func (wh *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	var body []byte
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err == nil {
			body = data
		}
	}
	if len(body) == 0 {
		MetricMutationBadRequest.Inc()
		log.WithField("id", id).Error("empty body")
		http.Error(w, "empty body", http.StatusBadRequest)
		return
	}
	var response *v1.AdmissionResponse
	ar := v1.AdmissionReview{}
	// decode the request
	if _, _, err := deserializer.Decode(body, nil, &ar); err != nil {
		MetricMutationBadRequest.Inc()
		log.WithError(err).WithField("id", id).Error("failed to decode body")
		response = &v1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	} else {
		// execute the mutation
		response = wh.mutate(r.Context(), &ar)
	}
	if response != nil {
		ar.Response = response
		if ar.Request != nil {
			ar.Response.UID = ar.Request.UID
		}
	}
	// response to kube
	resp, err := json.Marshal(&ar)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to marshall response")
		http.Error(w, "failed to marshall response", http.StatusInternalServerError)
		return
	}
	log.WithField("id", id).Info("ready to write response")
	w.Header().Set("Content-Type", "application/json")
	if _, err := w.Write(resp); err != nil {
		log.WithError(err).WithField("id", id).Error("failed to write response")
		http.Error(w, "failed to write response", http.StatusInternalServerError)
	}
}
