package webhook

import (
	"context"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

var allowTests = []struct {
	name     string
	metadata *metav1.ObjectMeta
	pass     bool
}{
	{
		"default namespace is allowed",
		&metav1.ObjectMeta{
			Namespace: metav1.NamespaceDefault,
		},
		true,
	},
	{
		"system namespace is blocked",
		&metav1.ObjectMeta{
			Namespace: metav1.NamespaceSystem,
		},
		false,
	},
	{
		"public namespace is blocked",
		&metav1.ObjectMeta{
			Namespace: metav1.NamespacePublic,
		},
		false,
	},
	{
		"empty namespace is allowed",
		&metav1.ObjectMeta{
			Namespace: metav1.NamespaceNone,
		},
		true,
	},
}

func TestServer_isMutationAllowed(t *testing.T) {
	server := &Server{}
	for _, tt := range allowTests {
		t.Run(tt.name, func(t *testing.T) {
			assert.EqualValues(t, tt.pass, server.isMutationAllowed(context.TODO(), tt.metadata))
		})
	}
}

func TestNewServer(t *testing.T) {
	server := NewServer()
	assert.Zero(t, len(server.mutations))
}
