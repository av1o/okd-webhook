package webhook

import (
	corev1 "k8s.io/api/core/v1"
)

type Mutation interface {
	Patch(target *corev1.Pod) (patch []PatchOperation)
}

type PatchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}
