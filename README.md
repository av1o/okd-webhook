# OKD Webhook

This project provides a MutatingWebhookConfiguration designed to impose the same security
restrictions as the `restricted` OpenShift SecurityContextConstraint.

This means:

* Must run as a non-root (semi)random UID/GID
* Must drop dangerous capabilities
* Must not be privileged or allow escalation
* Must not use any host networking (`hostIPC`, `hostPID`, `hostNetwork`)

## Configuration

* `port` - the port to run on
* `debug` - enables debug logging
* `tlsCertFile` - path to the TLS certificate for the webhook server
* `tlsKeyFile` - path to the TLS key for the webhook server
* `runAsUserMin` - minimum allowed UID (default: `1500080000`)
* `runAsUserMax` - maximum allowed UID (default: `1500080000`)
* `runAsGroup` - required GID (default: `0`)
* `fsGroup` - required FSGroup (default: `1500080000`)

## Installation

1. Generate TLS certificate and key that can be used by the Kubernetes API server.
This should be done using Cert Manager however it can be done manually (e.g. via a Kubernetes `CertificateSigningRequest`)


// TODO information about deployment