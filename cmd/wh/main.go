package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/okd-webhook/pkg/mutate"
	"gitlab.com/av1o/okd-webhook/pkg/webhook"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	port := flag.Int("port", 8080, "webhook webhook port")
	debug := flag.Bool("debug", false, "enables debug logging")

	certFile := flag.String("tlsCertFile", "/etc/webhook/certs/cert.pem", "file containing the x509 Certificate for HTTPS.")
	keyFile := flag.String("tlsKeyFile", "/etc/webhook/certs/key.pem", "file containing the x509 private key")

	runAsUserMin := flag.Int64("runAsUserMin", 1500080000, "minimum allowed value for the UID")
	runAsUserMax := flag.Int64("runAsUserMax", 1500090000, "maximum allowed value for the UID")
	runAsGroup := flag.Int64("runAsGroup", 0, "allowed GID value")
	fsGroup := flag.Int64("fsGroup", 1500080000, "allowed FSGroup")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabled debug logging")
	}

	// read tls keypair
	pair, err := tls.LoadX509KeyPair(*certFile, *keyFile)
	if err != nil {
		log.WithError(err).Panic("failed to load x509 keypair")
		return
	}
	// configure the services
	svc := webhook.NewServer(&mutate.RestrictedMutator{
		RunAsUserMin: *runAsUserMin,
		RunAsUserMax: *runAsUserMax,
		RunAsGroup:   *runAsGroup,
		FSGroup:      *fsGroup,
	}, &mutate.NetworkMutator{})

	router := mux.NewRouter()
	router.Handle("/mutate", tracer.NewHandler(svc)).
		Methods(http.MethodPost).
		Headers("Content-Type", "application/json")
	router.Handle("/metrics", promhttp.Handler())
	router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"userAgent":  r.UserAgent(),
			"remoteAddr": r.RemoteAddr,
		}).Debug("answering probe")
	})

	// configure the webhook
	log.Infof("starting webhook on interface %d", *port)
	server := &http.Server{
		Addr: fmt.Sprintf(":%d", *port),
		TLSConfig: &tls.Config{
			Certificates:             []tls.Certificate{pair},
			PreferServerCipherSuites: true,
			MinVersion:               tls.VersionTLS13,
		},
		Handler: router,
	}

	// start server in new goroutine
	go func() {
		if err := server.ListenAndServeTLS("", ""); err != nil {
			log.WithError(err).Panic("failed to run HTTP(S) server")
		}
	}()

	// listen for OS signals
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan

	log.Infof("received OS shutdown signal...")
	_ = server.Shutdown(context.Background())
}
