module gitlab.com/av1o/okd-webhook

go 1.16

require (
	github.com/djcass44/go-tracer v0.2.0
	github.com/gorilla/mux v1.8.0
	github.com/namsral/flag v1.7.4-pre
	github.com/prometheus/client_golang v1.9.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	k8s.io/api v0.20.2
	k8s.io/apimachinery v0.20.2
)
